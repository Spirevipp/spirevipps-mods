package com.spirevipp.forgeOreDictionaryOres.lib;


public class Constants {

    public static final String MODID = "forgeOreDictionaryOres";
    public static final String MODNAME = "Forge OreDict Ores";
    public static final String VERSION = "Alpha 1.0";
    public static final String CLIENT_PROXY = "com.spirevipp.forgeOreDictionaryOres.proxy.ClientProxy";
    public static final String COMMON_PROXY = "com.spirevipp.forgeOreDictionaryOres.proxy.CommonProxy";
}
