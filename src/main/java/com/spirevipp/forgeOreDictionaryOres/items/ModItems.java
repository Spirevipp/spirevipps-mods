package com.spirevipp.forgeOreDictionaryOres.items;


import net.minecraft.item.Item;

public final class ModItems {

    public static Item ingotCopper;
    public static Item ingotTin;
    public static Item ingotAluminum;
    public static Item ingotLead;
    public static Item ingotSilver;
    public static Item ingotNickel;

    public static void init(){

        ingotCopper = new IngotCopper();
        ingotTin = new IngotTin();
        ingotAluminum = new IngotAluminum();
        ingotLead = new IngotLead();
        ingotSilver = new IngotSilver();
        ingotNickel = new IngotNickel();

    }










}
