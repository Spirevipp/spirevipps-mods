package com.spirevipp.forgeOreDictionaryOres.items;

import com.spirevipp.forgeOreDictionaryOres.lib.Constants;
import com.spirevipp.forgeOreDictionaryOres.modCreativeTab;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;

public class IngotCopper extends Item {

    private String name = "ingotCopper";

    public IngotCopper() {

        setUnlocalizedName(Constants.MODID + "_" + name);
        GameRegistry.registerItem(this, name);
        setCreativeTab(modCreativeTab.modTab);
        setTextureName(Constants.MODID + ":" + name);
    }
}

