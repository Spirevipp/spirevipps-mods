package com.spirevipp.forgeOreDictionaryOres;


import com.spirevipp.forgeOreDictionaryOres.blocks.ModBlocks;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;


public class modCreativeTab {

    public static CreativeTabs modTab = new CreativeTabs("modTab"){
        @Override public Item getTabIconItem(){
            return Item.getItemFromBlock(ModBlocks.oreCopper);
        }
    };

    public static void creativeTab() {



    }
}
