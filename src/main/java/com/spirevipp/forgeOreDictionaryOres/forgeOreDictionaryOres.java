package com.spirevipp.forgeOreDictionaryOres;

import com.spirevipp.forgeOreDictionaryOres.blocks.ModBlocks;
import com.spirevipp.forgeOreDictionaryOres.items.ModItems;
import com.spirevipp.forgeOreDictionaryOres.lib.Constants;
import com.spirevipp.forgeOreDictionaryOres.proxy.CommonProxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

import static cpw.mods.fml.common.Mod.EventHandler;

@Mod(modid = Constants.MODID, name = Constants.MODNAME, version = Constants.VERSION)

public class forgeOreDictionaryOres {

    @SidedProxy(clientSide = Constants.CLIENT_PROXY, serverSide = Constants.COMMON_PROXY)
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {

        ModBlocks.init();
        ModItems.init();
        modCreativeTab.creativeTab();
        GameRegistry.registerWorldGenerator(new forgeOreDictionaryOreGen(), 10);

    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {

        proxy.registerTileEntities();

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }





}
