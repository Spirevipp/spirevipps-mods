package com.spirevipp.forgeOreDictionaryOres.blocks;


import com.spirevipp.forgeOreDictionaryOres.lib.Constants;
import com.spirevipp.forgeOreDictionaryOres.modCreativeTab;
import com.spirevipp.forgeOreDictionaryOres.tileentities.TileEntityCrusher;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class Crusher extends Block implements ITileEntityProvider{

    public static String name = "crusher";

    private String privateName = "crusher";

    public Crusher() {

        super(Material.rock);
        this.setBlockName(Constants.MODID + "_" + privateName);
        this.setCreativeTab(modCreativeTab.modTab);
        GameRegistry.registerBlock(this, privateName);

    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityCrusher();
    }

    @Override
    public boolean hasTileEntity(int metadata) {
        return true;
    }
}
