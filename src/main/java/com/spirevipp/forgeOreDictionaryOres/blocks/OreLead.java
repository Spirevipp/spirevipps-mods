package com.spirevipp.forgeOreDictionaryOres.blocks;


import com.spirevipp.forgeOreDictionaryOres.lib.Constants;
import com.spirevipp.forgeOreDictionaryOres.modCreativeTab;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class OreLead extends Block {

    private String name = "oreLead";

    public OreLead() {

        super(Material.rock);
        this.setBlockName(Constants.MODID + "_" + name);
        this.setCreativeTab(modCreativeTab.modTab);
        GameRegistry.registerBlock(this, name);
        setBlockTextureName(Constants.MODID + ":" + name);
    }
}
