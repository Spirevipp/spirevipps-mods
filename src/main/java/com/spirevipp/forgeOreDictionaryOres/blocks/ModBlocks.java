package com.spirevipp.forgeOreDictionaryOres.blocks;


import net.minecraft.block.Block;

public final class ModBlocks {

    public static Block oreCopper;
    public static Block oreTin;
    public static Block oreAluminum;
    public static Block oreLead;
    public static Block oreNickel;
    public static Block oreSilver;

    public static void init(){

        oreCopper = new OreCopper();
        oreTin = new OreTin();
        oreAluminum = new OreAluminum();
        oreLead = new OreLead();
        oreNickel = new OreNickel();
        oreSilver = new OreSilver();

    }










}
