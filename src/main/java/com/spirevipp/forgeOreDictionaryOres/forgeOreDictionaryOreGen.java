package com.spirevipp.forgeOreDictionaryOres;

import com.spirevipp.forgeOreDictionaryOres.blocks.ModBlocks;
import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;

import java.util.Random;


public class forgeOreDictionaryOreGen implements IWorldGenerator {

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
        switch(world.provider.dimensionId) {
            case 0: generateOverworld(random, world, chunkX * 16, chunkZ * 16); break;
            case 1: generateEnd(random, world, chunkX * 16, chunkZ * 16); break;
            case -1: generateNether(random, world, chunkX * 16, chunkZ * 16); break;
            default: generateOverworld(random, world, chunkX * 16, chunkZ * 16);
        }

    }

    private void generateEnd(Random random, World world, int x, int z) {
    }

    private void generateNether(Random random, World world, int x, int z) {
    }

    private void generateOverworld(Random random, World world, int x, int z) {
        addOres(ModBlocks.oreCopper, world, random, x, z, 2, 8, 40, 0, 64);
        addOres(ModBlocks.oreAluminum, world, random, x, z, 2, 6, 24, 0, 32);
        addOres(ModBlocks.oreLead, world, random, x, z, 2, 6, 24, 0, 32);
        addOres(ModBlocks.oreNickel, world, random, x, z, 2, 4, 12, 0, 24);
        addOres(ModBlocks.oreSilver, world, random, x, z, 2, 5, 12, 0, 24);
        addOres(ModBlocks.oreTin, world, random, x, z, 2, 8, 32, 0, 64);


    }

    public void addOres(Block block, World world, Random random, int blockXPos, int blockZPos, int minVeinSize, int maxVeinSize, int chanceToSpawn, int minY, int maxY) {

        WorldGenMinable minable = new WorldGenMinable(block, minVeinSize + random.nextInt(maxVeinSize - minVeinSize), Blocks.stone);
        for(int i = 0; i < chanceToSpawn; i++){
            int posX = blockXPos + random.nextInt(15);
            int posZ = blockZPos + random.nextInt(15);
            int posY = minY + random.nextInt(maxY - minY);
            minable.generate(world, random, posX, posY, posZ);
        }


    }
}
