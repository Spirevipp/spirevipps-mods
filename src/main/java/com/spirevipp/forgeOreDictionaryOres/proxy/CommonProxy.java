package com.spirevipp.forgeOreDictionaryOres.proxy;


import com.spirevipp.forgeOreDictionaryOres.blocks.Crusher;
import com.spirevipp.forgeOreDictionaryOres.tileentities.TileEntityCrusher;
import cpw.mods.fml.common.registry.GameRegistry;

public class CommonProxy {

    public void registerTileEntities() {

        GameRegistry.registerTileEntity(TileEntityCrusher.class, TileEntityCrusher.name);

    }
}
